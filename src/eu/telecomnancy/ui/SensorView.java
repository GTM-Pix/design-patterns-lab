package eu.telecomnancy.ui;

import eu.telecomnancy.pattern.command.Invoker;
import eu.telecomnancy.pattern.command.commandeGetValue;
import eu.telecomnancy.pattern.command.commandeOff;
import eu.telecomnancy.pattern.command.commandeOn;
import eu.telecomnancy.pattern.command.commandeUpdate;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SensorView extends JPanel {
    private ISensor sensor;
    private commandeOn allume = new commandeOn(sensor);
    private commandeOff etteint = new commandeOff(sensor);
    private commandeUpdate maj = new commandeUpdate(sensor);
    private commandeGetValue recup = new commandeGetValue(sensor);
    
    private Invoker onControl = new Invoker(allume);
    private Invoker offControl = new Invoker(etteint);
    private Invoker upControl = new Invoker(maj);
    private Invoker getControl = new Invoker(recup);
    
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    	
    public SensorView(ISensor c) {
        
        this.sensor = c;
        
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
                sensor.on();
            	//onControl.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
                sensor.off();
            	//offControl.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
                try {
                    sensor.update();
                	//upControl.execute();
                    value.setText(String.valueOf(sensor.getValue()));
                	//value.setText(String.valueOf(getControl.execute()));
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }
}
