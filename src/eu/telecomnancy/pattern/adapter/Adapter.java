package eu.telecomnancy.pattern.adapter;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;


public class Adapter implements ISensor
{
	LegacyTemperatureSensor sensor = new LegacyTemperatureSensor();
	
	public Adapter(LegacyTemperatureSensor sonde)
	{
		this.sensor = sonde;
	}
	
	

	public void on()
	{
		if(this.sensor.getStatus() == false)
		{
			this.sensor.onOff();
		}
		
	}

    /**
     * Disable the sensor.
     */
    public void off()
    {
    	if(this.sensor.getStatus())
		{
			this.sensor.onOff();
		}
    }

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus()
    {
    	return this.sensor.getStatus();
    }

    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public void update() throws SensorNotActivatedException
    {
    	
    }

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException
    {
    	if (getStatus())
            return this.sensor.getTemperature();
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
	
}
