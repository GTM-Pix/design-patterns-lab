package eu.telecomnancy.pattern.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class DecoratorValeur implements ISensor{

	protected ISensor capteur;
	public DecoratorValeur(ISensor sondedecore)
	{
		this.capteur = sondedecore;
	}
	public double getValue() throws SensorNotActivatedException
	{
		return capteur.getValue();
	}
	
}
