package eu.telecomnancy.pattern.decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Arrondi extends DecoratorValeur{
	public Arrondi(ISensor capteur)
	{
		super(capteur);
	}

	public double getValue() throws SensorNotActivatedException
	{
		return (Math.round(capteur.getValue()));
	}

	public void on() {
		// TODO Auto-generated method stub
		capteur.on();
	}

	public void off() {
		// TODO Auto-generated method stub
		capteur.off();
	}

	public boolean getStatus() {
		// TODO Auto-generated method stub
		return capteur.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		capteur.update();
	}
}
