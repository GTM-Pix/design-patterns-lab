package eu.telecomnancy.pattern.proxy;
import java.util.Date;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class ProxyLog implements ISensor{
	private ISensor sonde=new TemperatureSensor();
	private Date Datedj = new Date();
	private boolean state;

	public void on()
	{	
		System.out.println("Date : " + String.valueOf(Datedj)+ ", methode appel�e : on(), valeur retourn�e : nothing");
		sonde.on();
	}
	
	public void off()
	{
		System.out.println("Date : " + String.valueOf(Datedj)+ ", methode appel�e : off(), valeur retourn�e : nothing");
		sonde.off();
	}
	
	public boolean getStatus()
	{
		this.state = this.sonde.getStatus();
		System.out.println("Date : " + String.valueOf(Datedj)+ ", methode appel�e : getStatus(), valeur retourn�e : "+String.valueOf(this.state));
		return this.sonde.getStatus();
	}
	
	public void update() throws SensorNotActivatedException
	{
		System.out.println("Date : " + String.valueOf(Datedj)+ ", methode appel�e : update(), valeur retourn�e : nothing");
		sonde.update();
	}
	
	public double getValue() throws SensorNotActivatedException
	{
		System.out.println("Date : " + String.valueOf(Datedj)+ ", methode appel�e : getValue(), valeur retourn�e : "+String.valueOf(sonde.getValue()));
		return sonde.getValue();
	}
}
