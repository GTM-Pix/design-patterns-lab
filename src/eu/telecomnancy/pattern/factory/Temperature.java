package eu.telecomnancy.pattern.factory;

import java.util.Random;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Temperature implements ISensor{

	boolean state;
    double value = 0;
    
    public void on() {
        state = true;
    }

    
    public void off() {
        state = false;
    }

    
    public boolean getStatus() {
        return state;
    }

    
    public void update() throws SensorNotActivatedException {
        if (state)
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
}
