package eu.telecomnancy.pattern.factory;

import eu.telecomnancy.sensor.ISensor;

public class SensorFactory {
	public ISensor getSensor(String sensorType){
		if(sensorType == null){
	         return null;
	      }		
	      if(sensorType.equalsIgnoreCase("TEMPERATURE")){
	         return new Temperature();
	         
	      } else if(sensorType.equalsIgnoreCase("PRESSION")){
	         return new Pression();
	      }
	      
	      return null;
	   }
}
