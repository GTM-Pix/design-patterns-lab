package eu.telecomnancy.pattern.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class commandeGetValue implements Command{
private ISensor sonde;
	
	public commandeGetValue(ISensor s)
	{
		this.sonde=s;
	}
	public Object execute() throws SensorNotActivatedException {
		return this.sonde.getValue();
	}
}
