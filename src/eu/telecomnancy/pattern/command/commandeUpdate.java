package eu.telecomnancy.pattern.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class commandeUpdate implements Command{
	private ISensor sonde;
	
	public commandeUpdate(ISensor s)
	{
		this.sonde=s;
	}
	public Object execute() throws SensorNotActivatedException{
		this.sonde.update();
		return null;
	}
}
