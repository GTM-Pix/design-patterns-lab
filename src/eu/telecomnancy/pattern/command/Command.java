package eu.telecomnancy.pattern.command;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface Command {
	Object execute() throws SensorNotActivatedException;
}
