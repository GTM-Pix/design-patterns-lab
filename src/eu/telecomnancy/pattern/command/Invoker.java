package eu.telecomnancy.pattern.command;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Invoker {
	private Command controler;
	
	public Invoker(Command c)
	{
		this.controler = c;
	}
	public Object execute() throws SensorNotActivatedException
	{
		return this.controler.execute();
	}
}
