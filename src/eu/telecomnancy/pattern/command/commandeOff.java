package eu.telecomnancy.pattern.command;

import eu.telecomnancy.sensor.ISensor;

public class commandeOff implements Command{
	private ISensor sonde;
	
	public commandeOff(ISensor s)
	{
		this.sonde=s;
	}
	public Object execute(){
		this.sonde.off();
		return null;
	}
}
