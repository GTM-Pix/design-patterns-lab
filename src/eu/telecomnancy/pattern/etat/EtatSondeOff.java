package eu.telecomnancy.pattern.etat;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

public class EtatSondeOff implements ISensor{
	
	private TemperatureSensor sonde;
	public EtatSondeOff(TemperatureSensor sonde) {
        if (null == sonde) {
                throw new IllegalArgumentException("L'etat d'un capteur a besoin d�tre associ� au capteur qu'il d�signe");
        }
        this.sonde = sonde;
        
	}
	
	public void update()
	{
		throw new IllegalStateException("La sonde doit �tre activ� avant d'aquerir une nouvelle valeur.");
	}

	public boolean getStatus() {
        return this.sonde.getStatus();
    }
	
	public double getValue() {
        throw new IllegalStateException("Le capteur doit �tre activ� pour obtenir sa valeur.");
    }

	public void on() {
		this.sonde.setEtat(new EtatSondeOn(sonde));
	}

	public void off() {
		this.sonde.off();
	}
	
}
