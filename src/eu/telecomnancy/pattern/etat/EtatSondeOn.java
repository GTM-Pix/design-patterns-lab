package eu.telecomnancy.pattern.etat;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class EtatSondeOn implements ISensor{

	private TemperatureSensor sonde;
	public EtatSondeOn(TemperatureSensor sensor) {
        if (null == sonde) {
                throw new IllegalArgumentException("L'etat d'un capteur a besoin d�tre associ� au capteur qu'il d�signe");
        }
        this.sonde = sensor;
        
	}
	
	public void on() {
		this.sonde.on();
		
	}

	public void off() {
		this.sonde.setEtat(new EtatSondeOff(sonde));
	}

	public boolean getStatus() {
		return sonde.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		this.sonde.update();
		
	}

	public double getValue() throws SensorNotActivatedException {
		return this.sonde.getValue();
	}

	
}
