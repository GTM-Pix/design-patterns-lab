package eu.telecomnancy;

import eu.telecomnancy.pattern.adapter.Adapter;
import eu.telecomnancy.pattern.decorator.Arrondi;
import eu.telecomnancy.pattern.decorator.Celcius;
import eu.telecomnancy.pattern.factory.SensorFactory;
import eu.telecomnancy.pattern.proxy.ProxyLog;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {


	public static void main(String[] args) {
	//ISensor sensor = new ProxyLog();
	SensorFactory factory = new SensorFactory();
	ISensor sonde1 = factory.getSensor("TEMPERATURE");
	ISensor sonde2 = factory.getSensor("PRESSION");
	sonde1 = new Celcius(sonde1);
	sonde1 = new ProxyLog();
	sonde2 = new ProxyLog();
	sonde2 = new Arrondi(sonde2);
	//ISensor sensor = new TemperatureSensor();
		/*LegacyTemperatureSensor vieilleSonde = new LegacyTemperatureSensor();
		ISensor sensor = new Adapter(vieilleSonde);*/
	
       new MainWindow(sonde2);
    }

}
